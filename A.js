import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';

export default class A extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>AAAAAAA</Text>
        <Button title = 'Route to D' onPress = {() => this.props.navigation.navigate('Stack2',{m : 'D'})} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
