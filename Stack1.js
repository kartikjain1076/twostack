import {createStackNavigator,createAppContainer} from 'react-navigation';
import A from './A';
import B from './B';

const Stack1 = createStackNavigator({
    A : A,
    B : B
},
{
    initialRouteName : 'A'
})


export default Stack1;