import {createStackNavigator} from 'react-navigation';
import Stack1 from './Stack1';
import Stack2 from './Stack2';

const App = createStackNavigator({
    Stack1 : Stack1,
    Stack2 : Stack2
},
{
    initialRouteName : 'Stack1'
})


export default App;